var tinhTong = function () {
  var n = document.getElementById("number-n").value * 1;
  var x = document.getElementById("number-x").value * 1;
  var tong = 0;
  var luyThua = 1;
  for (var i = 1; i <= n; i++) {
    luyThua = luyThua * x;
    tong += luyThua;
  }
  document.getElementById(
    "result"
  ).innerHTML = `Giá trị n: ${n} <br/> Giá trị x: ${x} <br/> Kết quả là: ${tong}`;
};
